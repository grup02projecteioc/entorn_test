<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Registre d'un saló de bellesa</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/nouSalo.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="/js/validacioFormularis.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/validacioFormularis.css">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>
        <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
        <script src="/js/home.js" type="text/javascript"></script>


      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">

          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                <li><i class="fa-solid fa-user"></i></li>
                <li><a href="logout">Tancar sessio</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->
          <main class="main-class">

            <div class="container">

              <!-- Formulari-->

              <form:form class="form-horizontal" id="salo" modelAttribute="salo" action="registreSaloProcess"
                method="POST">
                <div class="card">
                  <div class="card-header">
                    <h3>Registre saló</h3>
                  </div>

                  <div class="card-body">

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Nom del saló / perruqueria:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="nomSalo" name="nomSalo" id="nomSalo"
                          required="required" />
                        <form:errors path="nomSalo" style="color: red;" />
                        <span>${missatgeError}</span>
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Direccio:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="direccio" name="direccio"
                          id="direccio" /> ${missatgeDireccio}
                        <form:errors path="direccio" style="color: red;" required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Codi Postal:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="codiPostal" name="codiPostal"
                          id="codiPostal" />
                        <form:errors path="codiPostal" style="color: red;" required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Ciutat:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="ciutat" name="ciutat" id="ciutat" />
                        <form:errors path="ciutat" style="color: red;" required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Numero de treballadors:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="numTreballadors"
                          name="numTreballadors" id="numTreballadors" />
                        <form:errors path="numTreballadors" style="color: red;" required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:input type="hidden" path="usuari" name="usuari" value="${usuari}" />
                    </div>

                    <div class="container-serveis">
                      <div class="row">
                        <div class="col-sm-12">
                          <h3>Afegeix els serveis que oferiras als clients</h3>
                        </div>
                        <div class="row">
                          <div>
                            <c:forEach items="${serveis}" var="serveis" varStatus="loop">
                              <ul class="list-goup">
                                <li class="list-group-item">
                                  <input type="checkbox" name="servei" value="${serveis.nomTipusServei}">
                                  ${serveis.nomTipusServei}<br />
                                </li>
                              </ul>
                            </c:forEach>

                          </div>
                        </div>
                      </div>
                      <div class="buttons">
                        <p>
                          <form:button id="guardar" name="guardar" class="btn">Guardar</form:button>
                        </p>
                        <p>
                          <a href="/benvingut" class="btn">Tornar</a>
                        </p>
                      </div>
                    </div>
                  </div>
              </form:form>
            </div>
          </main>
          <!-- Footer -->

          <!--seccio consentiment banner-->
          <div class="cookie-banner">
            <div class="cookie-close accept-cookie"></div>
            <div class="container">
              <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres
                serveis.
                <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s
                  infomaci&#243; sobre
                  la protecci&#243; de dades</a>
              </p>
              <button type="button" class="btn acceptar-cookie">Aceptar</button>
            </div>
          </div>


          <footer id="main-footer" class="footer">
            <div class="footer-inner">
              <div><img src="/img/Dark-logo.png" class="logo"></div>
              <ul>
                <li><a href="#">Contacte</a></li>
                <li><a href="#">Politica de la Web</a></li>
                <li><a href="#">&copy; Beauty Service 2022</a></li>
              </ul>
            </div>

          </footer>

        </div>

      </body>

      </html>