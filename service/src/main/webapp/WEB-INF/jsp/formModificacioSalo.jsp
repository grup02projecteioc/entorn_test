<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="/css/style.css">
            <link rel="stylesheet" href="/css/lesMevesDades.css">
            <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
            <script src="/js/validacioFormularis.js" type="text/javascript"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
            <link rel="stylesheet" href="/css/consentimentBanner.css">
            <script src="/js/cookies.js" type="text/javascript"></script>
            <script src="/js/consentimentBanner.js" type="text/javascript"></script>

            <title>Modificar dades saló</title>
        </head>

        <body>

            <!-- Responsive menu -->

            <div class="menu-btn">
                <i class="fas fa-bars fa-2x"></i>
            </div>

            <div class="container">

                <!-- Header -->
                <header id="home-header">
                    <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                    <nav class="main-nav">
                        <ul class="main-menu">
                            <li><a href="#">Home</a></li>
                            <li><a href="quiSom">Qui Som</a></li>
                            <li><a href="buscarReserva">Buscar Tractament</a></li>
                        </ul>
                        <ul class="right-menu">
                            <li><a href="#">Hola ${nomUsuari}</a></li>
                            <li><i class="fa-solid fa-user"></i></li>
                            <li><a href="logout">Tancar sessio</a></li>
                        </ul>
                    </nav>
                </header>

                <!-- Main area-->
                <main>

                    <section class="main-section">

                        <!-- My Profile Menu -->

                        <section id="lateral-menu">
                            <div>
                                <nav class="main-menu">
                                    <ul class="menuLateral">
                                        <a href="benvingut">Les meves solicituds pendents</a>
                                        <a href="elsMeusSalons">Els meus salons</a>
                                        <a href="lesMevesDades">Les meves dades</a>
                                        <a href="showRegistreSalo">Afegir un nou salo</a>
                                        <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                        <a href="baixa">Donar-te de baixa</a>
                                    </ul>
                                </nav>
                            </div>
                        </section>

                        <!-- Responsive Profile Menu-->
                        <div class="profile-menu">
                            <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                            <div id="myDropdown" class="dropdown-content">
                                <a href="benvingut">Les meves solicituds pendents</a>
                                <a href="elsMeusSalons">Els meus salons</a>
                                <a href="lesMevesDades">Les meves dades</a>
                                <a href="showRegistreSalo">Afegir un nou salo</a>
                                <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                <a href="baixa">Donar-te de baixa</a>
                            </div>
                        </div>

                        <section id="dades">
                            <div class="dades">
                                <h3>Modifica les teves dades:</h3>

                                <form:form id="formModificacioSalo" action="/modificacioSaloProcess"
                                    modelAttribute="salo" method="POST">
                                    <table>
                                        <tr>
                                            <th>
                                                <form:label path="nomSalo" class="control-label col-sm-4">Nom salo:
                                                </form:label>
                                            </th>
                                            <td>
                                                <form:input type="text" class="validate form-control" path="nomSalo"
                                                    name="nomSalo" id="nom" value="${nomSalo}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <form:label path="direccio">Direccio:</form:label>
                                            </th>
                                            <td>
                                                <form:input type="text" class="validate form-control" path="direccio"
                                                    name="direccio" id="direccio" value="${direccio}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <form:label path="ciutat">Ciutat:</form:label>
                                            </th>
                                            </th>
                                            <td>
                                                <form:input type="text" class="validate form-control" path="ciutat"
                                                    name="ciutat" id="ciutat" value="${ciutat}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <form:label path="codiPostal">Codi Postal:</form:label>
                                            </th>
                                            <td>
                                                <form:input type="text" class="validate form-control" path="codiPostal"
                                                    name="codiPostal" id="codiPostal" value="${codiPostal}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <form:label path="numTreballadors">Numero de treballadors:</form:label>
                                            </th>
                                            <td>
                                                <form:input type="text" class="validate form-control"
                                                    path="numTreballadors" name="numTreballadors" id="numTreballadors"
                                                    value="${numTreballadors}" />
                                            </td>
                                            <form:input type="hidden" path="idSalo" name="idSalo" id="idSalo"
                                                value="${idSalo}" />
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="butons">
                                        <form:button id="registre" name="registre" class="btn">Guardar canvis
                                        </form:button>

                                </form:form>
                            </div>
                        </section>
                    </section>
                </main>



                <!--Footer-->
                <footer id="main-footer" class="footer">
                    <div class="footer-inner">
                        <div><img src="/img/Dark-logo.png" class="logo"></div>
                        <ul>
                            <li><a href="#">Contacte</a></li>
                            <li><a href="#">Politica de la Web</a></li>
                            <li><a href="#">&copy; Beauty Service 2022</a></li>
                        </ul>
                    </div>
                </footer>
            </div>
        </body>

        </html>