




<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/home.css">
<script src="/js/home.js" type="text/javascript"></script>
<link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
<script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
<link href="css/reservesCalendari.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/css/consentimentBanner.css">
<script src="/js/cookies.js" type="text/javascript"></script>
<script src="/js/consentimentBanner.js" type="text/javascript"></script> 
<link href="icons/style.css" rel="stylesheet" type="text/css">
<title>Calendari Reserves</title>
</head>
<body>
  <!-- Responsive menu -->

  <div class="menu-btn">
    <i class="fas fa-bars fa-2x"></i>
  </div>

  <div class="container">
    <!-- Header -->
  <header id="home-header">
      <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

      <nav class="main-nav">  
        <ul class="main-menu">
          <li><a href="/">Home</a></li>
          <li><a href="#">Qui Som</a></li>
          <li><a href="buscarReserva">Reservar tractament</a></li>
        </ul>
        <ul class="right-menu">
          <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
          <li><i class="fa-solid fa-user"></i></li>
          <li><a href="logout">Tancar sessio</a></li>
        </ul>
      </nav>
  </header>



    <div class="main" style="display: flex;">
        <div style="margin-right: 10px;">
            <div id="nav"></div>
        </div>
        <div style="flex-grow: 1">
            <div id="dp"></div>
        </div>
    </div>
    
    <!-- DayPilot library -->
    <script src="js/daypilot/daypilot-all.min.js"></script>
        <!-- DayPilot calendari personalizat-->
    <script src="js/daypilot/calendariCitesSalo.js"></script>
    <!--Footer-->
    <footer id="main-footer" class="footer" style="  clear: both;position: relative; height: 200px;
    margin-top: -200px;">
      <div class="footer-inner">
        <div><img src="/img/Dark-logo.png" class="logo"></div>
        <ul>
          <li><a href="#">Contacte</a></li>
          <li><a href="#">Politica de la Web</a></li>
          <li><a href="#">&copy; Beauty Service 2022</a></li>
        </ul>
      </div>
      
    </footer>

  </div>
  
</body>
</html>
