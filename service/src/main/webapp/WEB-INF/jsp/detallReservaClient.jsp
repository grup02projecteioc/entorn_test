<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/resumReserva.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <link rel="stylesheet" href="/css/consentimentBanner.css">
                <script src="/js/cookies.js" type="text/javascript"></script>
                <script src="/js/consentimentBanner.js" type="text/javascript"></script>
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <title>Les meves reserves</title>
            </head>

            <body>

                <!-- Responsive menu -->

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">
                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="#">Qui Som</a></li>
                                <li><a href="buscarReserva">Reservar tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                                <li><i class="fa-solid fa-user"></i></li>
                                <li><a href="logout">Tancar sessio</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!-- Main area-->
                    <main>

                        <section class="main-section">

                            <!-- My Profile Menu -->

                            <section id="lateral-menu">
                                <div>
                                    <nav class="main-menu">
                                        <ul class="menuLateral">
                                            <li><a href="buscarReserva">Cercar un servei per reservar</a></li>
                                            <li><a href="lesMevesReserves">Les meves reserves</a></li>
                                            <li><a href="lesMevesDades">Les meves dades</a> </li>
                                            <li><a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a></li>
                                            <li><a href="baixa">Donar-te de baixa</a></li>
                                        </ul> 
                                    </nav>                                                                  
                                </div>
                            </section>

                            <!-- Responsive Profile Menu-->
                            <div class="profile-menu">
                                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="buscarReserva">Cercar un servei per reservar</a>
                                    <a href="lesMevesReserves">Les meves reserves</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </div>                           
                            </div>

                            <!-- Reserves -->

                            <section id="reserves-cards" class="reserves-cards">
                                <div class="reserves">
                                    <h3>La meva solicitud de reserva</h3>
                                    <img src="/img/skincare.jpg" alt="skincare">
                                    <div class="card-content">
                                        <tr>
                                            <h3 class="card-title">Reserva solicitada</h3>
                                            <p>Has solicitat el servei ${servei} al salo ${salo}</p>
                                            <p>el dia <input type="text" disabled id="data">a les <input type="text"
                                                    disabled id="hora"></p>
                                            <h4>Estat de la reserva: <input type="text" disabled id="estat"></h4>
                                            <c:choose>
                                                <c:when test="${estat == '2'}">
                                                    <a href="eliminarSolicitudClient?idReserva=${idReserva}" style="color:red">Eliminar del llistat</a>
                                                </c:when>
                                            </c:choose>                                          
                                        </tr>

                                        <a href="/cancelarSolicitud?idReserva=${idReserva}" class="btn">Cancelar sol·licitud</a>
                                        <a href="/lesMevesReserves" class="btn">Les meves reserves</a>
                                    </div>
                                </div>
                            </section>

                    </main>

                    <!--Footer-->
                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>

                    </footer>

                </div>
                <script>
                    let data = '${data}';
                    let arrayData = data.split("T");
                    let mesDia = arrayData[0];
                    let hora = arrayData[1];
                    let arrayMesDia = mesDia.split("-");
                    let mes = arrayMesDia[1];
                    let dia = arrayMesDia[2];
                    let inputData = document.getElementById("data");
                    let inputHora = document.getElementById("hora");
                    inputData.value = dia + " - " + mes;
                    inputHora.value = hora + " hores"

                    let estatReserva = '${estat}';
                    let inputEstat = document.getElementById("estat");
                    if (estatReserva == "0") {
                        inputEstat.value = "EN ESPERA";
                    } else if (estatReserva == "1") {
                        inputEstat.value = "ACCEPTADA";
                    } else {
                        inputEstat.value = "DECLINADA";
                    }
                </script>
            </body>

            </html>