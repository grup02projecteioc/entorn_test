<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/tramitacioSolicitudOK.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <script src="/js/cookies.js" type="text/javascript"></script>
                <link rel="stylesheet" href="/css/consentimentBanner.css">
                <script src="/js/consentimentBanner.js" type="text/javascript"></script>
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <title>Tramitar solicitud</title>
            </head>

            <body>

                <!-- Responsive menu -->

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">
                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="quiSom">Qui Som</a></li>
                                <li><a href="buscarReserva">Reservar tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                                <li><i class="fa-solid fa-user"></i></li>
                                <li><a href="logout">Tancar sessio</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!-- Main area-->
                    <main>

                        <section class="main-section">

                            <!-- My Profile Menu -->

                            <section id="lateral-menu">
                                <div>
                                    <nav class="main-menu">
                                        <ul class="menuLateral">
                                            <a href="benvingut">Les meves solicituds pendents</a>
                                            <a href="elsMeusSalons">Els meus salons</a>
                                            <a href="lesMevesDades">Les meves dades</a>
                                            <a href="showRegistreSalo">Afegir un nou salo</a>
                                            <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                            <a href="baixa">Donar-te de baixa</a>
                                        </ul>
                                    </nav>
                                </div>
                            </section>

                            <!-- Responsive Profile Menu-->
                            <div class="profile-menu">
                                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="benvingut">Les meves solicituds pendents</a>
                                    <a href="elsMeusSalons">Els meus salons</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="showRegistreSalo">Afegir un nou salo</a>
                                    <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </div>
                            </div>

                            <!-- Reserves -->

                                <div class="content">
                                    <section id="reserves-cards" class="reserves-cards">
                                        <div class="reserves">
                                            <h3>Solicitud tramitada!</h3>
                                            <p>${missatge}</p>
                                            <div>
                                                <img src="/img/Solicitud_OK.jpg" alt="Enhorabona">
                                                <div class="card-content">
                                                    <tr>
                                                        <h3 class="card-title">${servei}</h3>
                                                        <p>Solicitud realitzada per el dia <input type="text" disabled
                                                                id="data">a les <input type="text" disabled id="hora"></p>
                                                        <h4>Nom del client: <input type="text" disabled id="nom" value="${nom}">
                                                        </h4>
                                                    </tr>
        
                                                    <a href="solicitudsPendents?idSalo=${idSalo}" class="btn">Tornar</a>
                                                </div>
                                            </div>
                                        </div>
        
                                    </section>
                                </div>
                            

                    </main>

                    <!--seccio consentiment de banner-->
                    <div class="cookie-banner">
                        <div class="cookie-close accept-cookie"></div>
                        <div class="container">
                            <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament
                                dels nostres serveis. <br>Si accepteu o continueu navegant, considerem que accepteu les
                                condicions.<br>
                                <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per
                                    m&#233;s infomaci&#243; sobre la protecci&#243; de dades</a>
                            </p>
                            <button type="button" class="btn acceptar-cookie">Aceptar</button>
                        </div>
                    </div>

                    <!--Footer-->
                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>

                    </footer>

                </div>
                <script>
                    let data = '${data}';
                    let arrayData = data.split("T");
                    let mesDia = arrayData[0];
                    let hora = arrayData[1];
                    let arrayMesDia = mesDia.split("-");
                    let mes = arrayMesDia[1];
                    let dia = arrayMesDia[2];
                    let inputData = document.getElementById("data");
                    let inputHora = document.getElementById("hora");
                    inputData.value = dia + " - " + mes;
                    inputHora.value = hora + " hores"

                    let estatReserva = '${estat}';
                    let inputEstat = document.getElementById("estat");
                    if (estatReserva == "0") {
                        inputEstat.value = "EN ESPERA";
                    } else {
                        inputEstat.value = "ACCEPTADA";
                    }
                </script>
            </body>

            </html>