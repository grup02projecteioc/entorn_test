<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/benvingutGestor.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <link rel="stylesheet" href="/css/consentimentBanner.css">
                <script src="/js/cookies.js" type="text/javascript"></script>
                <script src="/js/consentimentBanner.js" type="text/javascript"></script>
                <title>Welcome</title>
            </head>

            <body>

                <!-- Responsive menu -->

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">

                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="quiSom">Qui Som</a></li>
                                <li><a href="buscarReserva">Reservar tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                                <li><i class="fa-solid fa-user"></i></li>
                                <li><a href="logout">Tancar sessio</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!-- Main area-->
                    <main>

                        <section class="main-section">

                            <!-- My Profile Menu -->

                            <section id="lateral-menu">
                                <div>
                                    <nav class="main-menu">
                                        <ul class="menuLateral">
                                            <a href="benvingut">Les meves solicituds pendents</a>
                                            <a href="elsMeusSalons">Els meus salons</a>
                                            <a href="lesMevesDades">Les meves dades</a>
                                            <a href="showRegistreSalo">Afegir un nou salo</a>
                                            <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                            <a href="baixa">Donar-te de baixa</a>
                                        </ul>
                                    </nav>
                                </div>
                            </section>

                            <!-- Responsive Profile Menu-->
                            <div class="profile-menu">
                                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="benvingut">Les meves solicituds pendents</a>
                                    <a href="elsMeusSalons">Els meus salons</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="showRegistreSalo">Afegir un nou salo</a>
                                    <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </div>
                            </div>

                            <!-- Llistat Salons -->

                            <section id="llistat" class="llistat">
                                <img src="/img/Meus_salons.jpg" alt="Saló">
                                <div>
                                    <c:choose>
                                        <c:when test="${fn:length(salons) == '0'}">
                                            <p>Encara no tens salons</p>
                                            <a href="showRegistreSalo" class="btn">Afegir un nou salo</a>
                                        </c:when>
                                        <c:otherwise>
                                            <form action="gestionarSalo" method="get">
                                                <c:forEach items="${salons}" var="salons" varStatus="loop">
                                                    <tr>
                                                        <a href="gestionarSalo?idSalo=${salons.idSalo}">
                                                            <h4>${salons.nomSalo}</h4>
                                                        </a><br>
                                                        <td>${salons.direccio}</td>
                                                        <td><a href="calendariCitesSalo">Calendari de cites</a></td>
                                                        <br>
                                                        <a class="btn"
                                                            href="solicitudsPendents?idSalo=${salons.idSalo}">Solicituds
                                                            pendents</a> <br />
                                                        <a class="btn"
                                                            href="solicitudsAcceptades?idSalo=${salons.idSalo}">Solicituds
                                                            acceptades</a> <br />
                                                        <a class="btn"
                                                            href="gestionarSalo?idSalo=${salons.idSalo}">Gestionar
                                                            salo</a> <br />
                                                    </tr>
                                                </c:forEach>
                                            </form>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </section>
                        </section>

                    </main>

                    <!--seccio consentiment banner-->
                    <div class="cookie-banner">
                        <div class="cookie-close accept-cookie"></div>
                        <div class="container">
                            <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament
                                dels nostres serveis.
                                <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                                <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per
                                    m&#233;s infomaci&#243; sobre
                                    la protecci&#243; de dades</a>
                            </p>
                            <button type="button" class="btn acceptar-cookie">Aceptar</button>
                        </div>
                    </div>

                    <!--Footer-->

                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>
                    </footer>

                </div>

            </body>

            </html>