<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/benvingut.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <link rel="stylesheet" href="/css/consentimentBanner.css">
                <script src="/js/cookies.js" type="text/javascript"></script>
                <script src="/js/consentimentBanner.js" type="text/javascript"></script>
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <title>Welcome</title>
            </head>

            <body>

                <!-- Responsive menu -->

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">
                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="quiSom">Qui Som</a></li>
                                <li><a href="buscarReserva">Reservar tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                                <li><i class="fa-solid fa-user"></i></li>
                                <li><a href="logout">Tancar sessio</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!-- Main area-->
                    <main>

                        <section class="main-section">

                            <!-- My Profile Menu -->
                            <section id="lateral-menu">
                                <div>
                                    <nav class="main-menu">
                                        <ul class="menuLateral">
                                            <li><a href="buscarReserva">Cercar un servei per reservar</a></li>
                                            <li><a href="lesMevesReserves">Les meves reserves</a></li>
                                            <li><a href="lesMevesDades">Les meves dades</a> </li>
                                            <li><a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a></li>
                                            <li><a href="baixa">Donar-te de baixa</a></li>
                                        </ul> 
                                    </nav>                                                                  
                                </div>
                            </section>

                            <!-- Responsive Profile Menu-->
                            <div class="profile-menu">
                                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="buscarReserva">Cercar un servei per reservar</a>
                                    <a href="lesMevesReserves">Les meves reserves</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </div>                           
                            </div>

                            <!-- Reserves -->
                            <section class="reserves-card">
                                <c:choose>
                                    <c:when test="${fn:length(reserves) == '0'}">
                                        <div>
                                            <p>Encara no tens reserves</p>
                                            <a href="buscarReserva">Buscar un servei per reservar</a>
                                        </div>
                                        
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${fn:length(reservesConfirmades) == '0'}">
                                                <p>No tens cap reserva confirmada</p>
                                            </c:when>
                                            <c:otherwise>
                                                <section id="reserves-cards" class="reserves-cards">
                                                    <div class="reserves">
                                                    <h3>Les meves reserves confirmades</h3>
                                                </div>
                                                <div>   
                                                    <div class="card-content">
                                                        <c:forEach items="${reservesConfirmades}" var="reservesConfirmades" varStatus="loop">
                                                            <tr>
                                                                <h3 class="card-title">Reserva confirmada</h3>
                                                                <img src="/img/skincare.jpg" alt="skincare">
                                                                <p>Has solicitat el servei ${reservesConfirmades.text}</p>
                                                                <h4><a class="btn"
                                                                        href="detallReservaClient?idSalo=${reservesConfirmades.idSalo}&idReserva=${reservesConfirmades.idReserva}">Detall
                                                                        de la reserva</a></h4>
                                                            </tr>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                                </section>
                                            </c:otherwise>
                                        </c:choose>
                                       
                                        
                                        <c:choose>
                                            <c:when test="${fn:length(reservesPendents) == '0'}">
                                                <p>No tens cap reserva pendent de confirmar</p>
                                            </c:when>
                                            <c:otherwise>
                                                <section id="reserves-cards" class="reserves-cards">
                                                    <div class="reserves">
                                                        <h3>Les meves reserves pendents de confirmar</h3>
                                                    </div>
                                                    <div>   
                                                        <div class="card-content">
                                                            <c:forEach items="${reservesPendents}" var="reservesPendents" varStatus="loop">
                                                                <tr>
                                                                    <h3 class="card-title">Reserva solicitada</h3>
                                                                    <img src="/img/skincare.jpg" alt="skincare">
                                                                    <p>Has solicitat el servei ${reservesPendents.text}</p>
                                                                    <h4><a class="btn"
                                                                            href="detallReservaClient?idSalo=${reservesPendents.idSalo}&idReserva=${reservesPendents.idReserva}">Detall
                                                                            de la reserva</a></h4>
                                                                </tr>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </section>
                                            </c:otherwise>
                                        </c:choose> 
                                        

                                    </c:otherwise>                                 
                                </c:choose>
                                <c:choose>
                                    <c:when test="${fn:length(reservesDescartades) > '0'}">
                                        <h4>T'han descartat les següents solicituds:</h4>
                                        <c:forEach items="${reservesDescartades}" var="reservesDescartades" varStatus="loop">
                                                <p><a href="detallReservaClient?idSalo=${reservesDescartades.idSalo}&idReserva=${reservesDescartades.idReserva}">Servei ${reservesDescartades.text}</p>
                                        </c:forEach>
                                    </c:when>
                                </c:choose> 
                            </section>                     
                    </main>

                    <!--seccio consentiment banner-->
                    <div class="cookie-banner">
                        <div class="cookie-close accept-cookie"></div>
                        <div class="container">
                          <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres serveis.
                            <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                            <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s infomaci&#243; sobre
                              la protecci&#243; de dades</a>
                          </p>
                          <button type="button" class="btn acceptar-cookie">Aceptar</button>
                        </div>
                      </div>                    

                    <!--Footer-->
                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>

                    </footer>

                </div>

            </body>

            </html>