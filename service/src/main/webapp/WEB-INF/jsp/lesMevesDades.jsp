<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
            <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/lesMevesDades.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>
        <script src="/js/home.js" type="text/javascript"></script>

        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <title>Les Meves Dades</title>
    </head>

    <body>

        <!-- Responsive menu -->

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">

            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="quiSom">Qui Som</a></li>
                        <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                        <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                        <li><i class="fa-solid fa-user"></i></li>
                        <li><a href="logout">Tancar sessio</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Main area-->
            <main>

                <section class="main-section">

                    <!-- My Profile Menu -->

                    <section id="lateral-menu">
                        <div>
                            <nav class="main-menu">
                                <ul class="menuLateral">
                                    <c:choose>
                                        <c:when test="${rol == 'CLIENT'}">
                                            <li><a href="buscarReserva">Cercar un servei per reservar</a></li>
                                            <li><a href="lesMevesReserves">Les meves reserves</a></li>
                                            <li><a href="lesMevesDades">Les meves dades</a> </li>
                                            <li><a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a></li>
                                            <li><a href="baixa">Donar-te de baixa</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="benvingut">Les meves solicituds pendents</a>
                                            <a href="elsMeusSalons">Els meus salons</a>
                                            <a href="lesMevesDades">Les meves dades</a>
                                            <a href="showRegistreSalo">Afegir un nou salo</a>
                                            <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                            <a href="baixa">Donar-te de baixa</a>
                                        </c:otherwise>
                                    </c:choose>

                                </ul> 
                            </nav>                                                                  
                        </div>
                    </section>

                    <!-- Responsive Profile Menu -->
                    <div class="profile-menu">
                        <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                        <div id="myDropdown" class="dropdown-content">
                            <c:choose>
                                <c:when test="${rol == 'CLIENT'}">
                                    <a href="buscarReserva">Cercar un servei per reservar</a>
                                    <a href="lesMevesReserves">Les meves reserves</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </c:when>
                                <c:otherwise>
                                    <a href="benvingut">Les meves solicituds pendents</a>
                                    <a href="elsMeusSalons">Els meus salons</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="showRegistreSalo">Afegir un nou salo</a>
                                    <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </c:otherwise>
                            </c:choose>
                        </div>                           
                    </div>

                    <section id="dades">
                        <div class="dades">
                            <h3>Aquestes son les teves dades:</h3>
                        
                            <table>
                                <tr>
                                    <th>Nom:</th>
                                    <td>${nom}</td>
                                </tr>
                                <tr>
                                    <th>Cognoms:</th>
                                    <td>${cognoms}</td>
                                </tr>
                                <tr>
                                    <th>Email:</th>
                                    <td>${mail}</td>
                                </tr>
                                <tr>
                                    <th>Ciutat:</th>
                                    <td>${ciutat}</td>
                                </tr>
                                <tr>
                                    <th>Codi Postal:</th>
                                    <td>${codiPostal}</td>
                                </tr>                                
                            </table>

                            <div class="butons">
                                <a href="/modificarDades" class="btn">Modificar les meves dades</a>

                                <a href="logout" class="btn">Tancar sessio</a>

                                <a href="baixa" class="btn">Donar-me de baixa</a>
                            </div>

                        </div>


                    </section>

                </section>

                <!--Footer-->
                <footer id="main-footer" class="footer">
                    <div class="footer-inner">
                        <div><img src="/img/Dark-logo.png" class="logo"></div>
                        <ul>
                            <li><a href="#">Contacte</a></li>
                            <li><a href="#">Politica de la Web</a></li>
                            <li><a href="#">&copy; Beauty Service 2022</a></li>
                        </ul>
                    </div>

                </footer>
        </div>

    </body>

    </html>