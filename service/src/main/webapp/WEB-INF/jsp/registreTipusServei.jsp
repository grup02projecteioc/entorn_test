<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <jsp:useBean id="tipusServei" class="beauty.service.service.repositori.Entitats.TipusServeis" scope="session">
        </jsp:useBean>
        <title>Registre Gestor de Centre</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/registre.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="/js/validacioFormularis.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/validacioFormularis.css">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>        

      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                <li><a href="logout">Tancar sessio</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->
          <main>

            <div class="container-main">

              <div class="welcome">
              </div>

              <form:form class="form-horizontal" id="tipusServeis" modelAttribute="tipusServeis"
                action="registreTipusServeiProcess" method="POST">
                <div class="card">
                  <div class="card-header">
                    <h3>Registre tipus serveis</h3>
                  </div>

                  <div class="card-body">

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Nom del tipus de servei:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="nomTipusServei" name="nomTipusServei" id="nomTipusServei"  required="required"/>
                        <form:errors path="nomTipusServei"  style="color: red;"  />
                      </div><span>${missatgeError}</span>
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="" class="control-label col-sm-4">Descripcio&#769; del tipus de servei:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="descripcioTipus" name="descripcioTipus" id="descripcioTipus" />
                          <form:errors path="descripcioTipus" style="color: red;"  required="required"/>
                        </div><span>${missatgeGuardat}</span>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-4"><form:button id="Guardar" name="Guardar" class="btn">Guardar</form:button></div>
                      <div class="col-sm-4"><a href="llistarRegistreGestorTipusServei" class="btn">Llistar Serveis</a></div>
                        <div class="col-sm-4"><a href="/" class="btn">Home</a></div>
                      
                    </div>


                  </div>
                </div>
              </form:form>
            </div>            

            <!--Footer-->
            <footer id="main-footer" class="footer">
              <div class="footer-inner">
                <div><img src="/img/Dark-logo.png" class="logo"></div>
                <ul>
                  <li><a href="#">Contacte</a></li>
                  <li><a href="#">Politica de la Web</a></li>
                  <li><a href="#">&copy; Beauty Service 2022</a></li>
                </ul>
              </div>

            </footer>

        </div>

      </body>

      </html>