<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/home.css">
    <script src="/js/home.js" type="text/javascript"></script>

    <link rel="stylesheet" href="/css/consentimentBanner.css">
    <script src="/js/cookies.js" type="text/javascript"></script>
    <script src="/js/consentimentBanner.js" type="text/javascript"></script>

    <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
    <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
    <title>Home</title>
  </head>

  <body>
    <!-- Responsive menu -->

    <div class="menu-btn">
      <i class="fas fa-bars fa-2x"></i>
    </div>

    <div class="container">
      <!-- Header -->
      <header id="home-header">
        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

        <nav class="main-nav">
          <ul class="main-menu">
            <li><a href="/">Home</a></li>
            <li><a href="quiSom">Qui Som</a></li>
            <li><a href="buscarReserva">Reservar tractament</a></li>
          </ul>
          <ul class="right-menu">
            <li><a href="benvingut">${logat}</a></li>
            <li><a href="login">${login}</a></li>
            <li><i class="fa-solid fa-user"></i></li>
            <li><a href="tipusUsuari">${registre}</a></li>
            <li><a href="logout">${tancarSessio}</a></li>
          </ul>
        </nav>
      </header>

      <!-- Main area-->
      <main>
        <!--Section A-->
        <section id="Home-section-a" class="home-section-a">
          <a href="buscarReserva" class="btn">Reservar</a>
        </section>

        <!--Section B-->
        <section id="Home-section-b" class="home-cards">
          <div>
            <img src="/img/Sauna.jpg" alt="Sauna">
            <div class="card-content">
              <h3 class="card-title"><a href="buscarEspecific?nomTractament=Saunatradicional">Sauna tradicional</a></h3>
              <p>Bany de vapor que es realitza en una habitació a altes temperatures</p>
              <a href="buscarEspecific?nomTractament=Saunatradicional" class="btn">Reservar</a>
            </div>
          </div>
          <div>
            <img src="/img/skincare.jpg" alt="Skincare">
            <div class="card-content">
              <h3 class="card-title"><a href="buscarEspecific?nomTractament=TractamentDePellCarbonPeelFlash">Tractament
                  de pell Carbon Peel Flash</a></h3>
              <p>Cara més lluminosa i amb visible reducció de les imperfeccions</p>
              <a href="buscarEspecific?nomTractament=TractamentDePellCarbonPeelFlash" class="btn">Reservar</a>
            </div>
          </div>
          <div>
            <img src="/img/makeup-brushes.jpg" alt="Makeup">
            <div class="card-content">
              <h3 class="card-title"><a href="buscarEspecific?nomTractament=MaquillatgePerAFotos">Maquillatge per a
                  fotos</a></h3>
              <p>Maquillatge preparat especialment per les teves fotos especials</p>
              <a href="buscarEspecific?nomTractament=MaquillatgePerAFotos" class="btn">Reservar</a>
            </div>
          </div>
          <div>
            <img src="/img/massage-therapy.jpg" alt="Massage">
            <div class="card-content">
              <h3 class="card-title"><a href="buscarEspecific?nomTractament=MassatgeReafirmantEstimulant">Massatge
                  Reafirmant Estimulant</a></h3>
              <p>Millora en el to muscular i del teixit cutani</p>
              <a href="buscarEspecific?nomTractament=MassatgeReafirmantEstimulant" class="btn">Reservar</a>
            </div>
          </div>
        </section>

        <!--Section C-->
        <section id="Home-section-c" class="home-section-c">
          <div class="content">
          </div>
        </section>
      </main>

      <!--Footer-->
      <footer id="main-footer" class="footer">
        <div class="footer-inner">
          <div><img src="/img/Dark-logo.png" class="logo"></div>
          <ul>
            <li><a href="#">Contacte</a></li>
            <li><a href="#">Politica de la Web</a></li>
            <li><a href="#">&copy; Beauty Service 2022</a></li>
          </ul>
        </div>

      </footer>

    </div>

  </body>

  </html>