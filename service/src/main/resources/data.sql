-- S'afegeixen valors per defecte a la taula "usuari"
INSERT   INTO   beauty.usuari (active,ciutat,codi_postal, cognoms, contrasenya, mail, nom, nom_usuari, rol) VALUES
(1,"Badalona", "08910", "Badia Castellà","123","berta@mail.cat","Berta","berta","GESTOR"),
(1,"Badalona", "08910", "Castellnou Sarda","123","nuria@mail.cat","Nuria","nuria","GESTOR"),
(1,"Badalona", "08910", "Gallifa Armengol","123","joaquin@mail.cat","Joaquin","joaquin","GESTOR"),
(1,"Badalona", "08910", "Jane Muñoz","123","josep@mail.cat","Josep","josep","GESTOR"),
(1,"Barcelona", "08001", "Pinyol Roig","123","ferran@mail.cat","Ferran","ferran","GESTOR"),
(1,"Barcelona", "08001", "Subirana Catlla","123","pere@mail.cat","Pere","pere","GESTOR"),
(1,"Barcelona", "08001", "Pla Millan","123","pilar@mail.cat","Pilar","pilar","GESTOR"),
(1,"Barcelona", "08001", "Perarnau Riera","123","merce@mail.cat","Merce","merce","GESTOR"),
(1,"Granollers", "08400", "Lloreda Ambròs","123","joan@mail.cat","Joan","joan","GESTOR"),
(1,"Granollers", "08400", "Dilla Borras","123","miguel@mail.cat","Miguel","miguel","GESTOR"),
(1,"Granollers", "08400", "Dilme Quintana","123","esteban@mail.cat","Esteban","esteban","GESTOR"),
(1,"Granollers", "08400", "Dinares Prat","123","jaime@mail.cat","Jaime","jaime","GESTOR"),
(1,"Sabadell", "08200", "Ratia Francesc","123","victor@mail.cat","Victor","victor","GESTOR"),
(1,"Sabadell", "08200", "Dodas Font","123","santi@mail.cat","Santi","santi","GESTOR"),
(1,"Sabadell", "08200", "Domenec Corominas","123","montserrat@mail.cat","Montserrat","montserrat","GESTOR"),
(1,"Sabadell", "08200", "Domenech Dachs","123","merce@mail.cat","Merce","merce","GESTOR"),
(1,"Tarrasa", "08221", "Raubet Bigorda","123","jose@mail.cat","Jose","jose","GESTOR"),
(1,"Manresa", "08240", "Gallego Romero","123","rafael@mail.com","Rafael","rafael","GESTOR"),
(1,"Mataró", "08300", "Gallen Ibañez","123","julia@mail.com","Julia","julia","GESTOR"),
(1,"Hospitalet de Llobregat", "08900", "Gamero Higueruelo","123","alfonso@mail.com","Alfonso","alfonso","GESTOR"),
(1,"Castell-Platja d'Aro", "17248", "Moreno Ruiz","123","encarnacion@mail.com","Encarnacion","encarnacion","GESTOR"),
(1,"Sort", "25560", "Morera Bres","123","jordi@mail.com","Jordi","jordi","GESTOR"),

(1,"Badalona", "08910", "Domingo Alcazar","123","lidia@mail.cat","Lidia","lidia","CLIENT"),
(1,"Badalona", "08910", "Donadeu Maso","123","pilar@mail.cat","Pilar","pilar","CLIENT"),
(1,"Barcelona", "08001", "Dorado Villen","123","ramon@mail.cat","Ramon","ramon","CLIENT"),
(1,"Barcelona", "08001", "Doniga Fraile","123","benjamin@mail.cat","Benjamin","benjamin","CLIENT"),
(1,"Granollers", "08400", "Echarri Eguinoa","123","rosario@mail.cat","Rosario","rosario","CLIENT"),
(1,"Granollers", "08400", "Eguzquiza Rosal","123","concepcio@mail.cat","Concepcio","concepcio","CLIENT"),
(1,"Sabadell", "08200", "Erra Aligue","123","ana@mail.cat","Ana","ana","CLIENT"),
(1,"Sabadell", "08200", "Erra Fabre","123","josefa@mail.cat","Josefa","josefa","CLIENT"),
(1,"Tarrasa", "08221", "Escale Bertrans","123","pedro@mail.cat","Pedro","pedro","CLIENT"),
(1,"Manresa", "08240", "Escobar Guerrero","123","saida@mail.com","Saida","saida","CLIENT"),
(1,"Mataró", "08300", "Escribano Rodriguez","123","aquilino@mail.com","Aquilino","aquilino","CLIENT"),
(1,"Hospitalet de Llobregat", "08900", "Escudero Fernandez","123","jesualdo@mail.com","Jesualdo","jesualdo","CLIENT"),
(1,"Platja d'Aro", "17248", "Espinosa Gargantel","123","teresa@mail.com","Teresa","teresa","CLIENT"),
(1,"Sort", "25560", "Esteban Papiol","123","elvira@mail.com","Elvira","elvira","GESTOR");

-- Es deshabilita el mode segur el motor mysql per modificar registre registre Delete, Update, Truncate
-- sense especificar la clau a la clàusula Where.
SET SQL_SAFE_UPDATES = 0;
-- S'eliminen valors repetits, fent una comparació amb la mateixa taula a la columna, "nom_usuari", i comparant
-- la grandaria de la seva clau principal
DELETE u1 FROM beauty.usuari u1 INNER JOIN beauty.usuari  u2
ON u2.nom_usuari = u1.nom_usuari AND u2.id_usuari < u1.id_usuari;
/*
INSERT   INTO   beauty.salo (ciutat, cp, direccio, num_treballadors,nom_salo) VALUES
("Badalona",  "08910", "Carrer de Ramon i Cajal, N 6",4,"Blue Velvet"),
("Badalona",  "08910", "Carrer de Francesc Macià, 3",3,"Yolanda Cuesta"),
("Badalona",  "08910", "Passeig de la Salut, 107",9,"Tiqui"),
("Badalona",  "08910", "Avinguda del Marquès de Sant Morí, 118",1,"Petit Saló Badalona"),
("Badalona",  "08910", "Carretera de Santa Coloma, 56",9,"Perruquería Look"),
("Badalona",  "08910", "Carrer de la Muntanya, 18",5,"Cambiat Estilistas"),
("Barcelona", "08001", "Carrer de Brusi, 39", 5,"Care Beauty Place"),
("Barcelona", "08001", "Carrer de l'Associació, 41" ,8,"Edrian Styles"),
("Barcelona", "08001", "Carrer de Mallorca, 193" ,3,"Opera Lounges"),
("Barcelona", "08001", "Carrer de Calàbria, 234" ,4,"Ammate"),
("Barcelona", "08001", "Carrer Gabriel i Galán, 24" ,3,"Vanessa Hidalgo Estilistes"),
("Granollers","08400", "Plaça Lluis Perpinyà, 28" ,3,"Sol Estetica Perruqueria"),
("Granollers","08400", "Calle Ramón Llull, 5" ,2,"MAMIS&Kids"),
("Granollers","08400", "Miquel Ricoma, 36" ,3,"Carol Bruguera"),
("Granollers","08400", "Carrer Girona, 223" ,2,"Sebas & Co"),
("Granollers","08400", "Carrer Princesa, 3" ,3,"De Paris A Nueva York S.C.P."),
("Sabadell", "08200", "Carrer de Gràcia, 152" ,2,"Blens Perruquers"),
("Sabadell", "08200", "Carrer Duran i Sors, 3-5" ,8,"LeLook - Perruqueria d'Autor"),
("Sabadell", "08200", "c/Urgell Nº33" ,3,"Decrem Professional"),
("Sabadell", "08200", "c/Indústria, 35" ,2,"Attraction By Yelixa"),
("Sabadell", "08200", "Av. Barberà, 83" ,4,"Manuel Serran"),
("Tarrasa",  "08221", "c/ Nou de Sant Pere 11" ,2,"Perruqueries Querol"),
("Tarrasa",  "08221", "Cl. Rutlla, 33" ,1,"Labarias Perruquers"),
("Tarrasa",  "08221", "Pg. Vapor Gran, S/N",3,"Monalisa"),
("Tarrasa",  "08221", "Carrer Sant Pere, 53",5,"Depiline Center Terrassa"),
("Manresa", "08240", "Ctra. Vic, 25",2,"Carol Bruguera"),
("Manresa", "08240", "Pg. Pere III, 67",7,"Aquillue Perruquers"),
("Manresa", "08240", "Carrer  Còs, 38-40",4,"Perruqueria Maria Angels"),
("Manresa", "08240", "Carrer Pompeu Fabra, 6",2,"Els 60"),
("Manresa", "08240", "Carrer Carraco i Formiguera, 27, BAIXOS",3,"Iratxe Perruquers"),
("Mataró", "08300", "C/Nàpols, 29S",2,"Pili Luque Peluqueria"),
("Mataró", "08300", "Pg. Carles Padrós, 53",2,"Escabias Perruquers"),
("Hospitalet de Llobregat", "08900", "C/ Casa Nova, 74",5,"Peluqueria Atreeevete"),
("Hospitalet de Llobregat", "08900", "Carrer Enginyer Moncunill, 1",6,"Peluquería Estética A&m"),
("Platja d'Aro", "17248", "Galerias Sant LLuis,17",2,"HAIR LAB"),
("Platja d'Aro", "17248", "AV CASTELL D'ARO, 76",2,"ESTILISME PLATJA D'ARO"),
("Sort", "25560",  "Plaza Mayor, 3",2,"Perruquería María Pilar"),
("Sort", "25560",  "Passatge del Riuet, s/n",3,"La Peluka");

-- S'eliminen valors repetits, fent una comparació amb la mateixa taula a la columna, "nom_salo" i 
-- comparant la grandaria del id_salo
DELETE s1 FROM beauty.salo s1 INNER JOIN beauty.salo s2
ON s2.nom_salo = s1.nom_salo AND s2.id_salo < s1.id_salo;
-- Es torna habilitar el mode segur*/

INSERT   INTO   beauty.tipus_serveis (nom_tipus_servei,descripcio_tipus, id_salo) VALUES("Pentinat amb planxa", "Un acabat intermedi entre els tirabuixons i els cabells llisos o fins i tot fosc, ondulant la cabellera",null),
("Tall  llarg a capes", " És un tall que aporten volum a la cabellera",null),
("Tall serrell obert", "És tall lleugerament més curt al centre i més llarg a mesura que s'acosta a les temples, emmarcant el rostre.
",null),
("Bany de color", "Coloració per oxidació, que s'obtenen de combinar una crema colorant amb una reveladora per poder penetrar a la cutícula, treure pigment de color i dipositar-ne un de nou",null),
("Prepigmentació", "un procés que es fa als cabells amb el mateix tint per aconseguir que el color es fixi millor i duri més",null),
("Pentinat recollit", "Es realitzen separacions als cabells en parts, des de la part frontal -normalment- i de dalt a baix.
",null),
("Depilació Làser de Díode", "És un sistema de depilació definitiva. Per eliminar el pèl des de l'arrel, s'aplica un feix de llum que penetra a la pell sense fer-la malbé
",null),
("Fish Pedicure Ruham", "Aquest tractament consisteix a tenir més de 100 peixos Garra rufa, també coneguts com peix doctor o Doctor Fish, en una peixera de més de 120 litres d'aigua perquè així et facin un peeling natural i succionin tota la pell morta",null),
("Rentat Sunshine Experience", "Rentat, pentinat i tractament de brillantor.",null),
("Talls bob", "Estil de mitja cabellera curta, on la longitud dels laterals es troba per sobre del naixement del cabell a la zona posterior.",null),
("Tall Short bob clàssic", "Elegant i refinat, es caracteritza per la llargada a l'alçada de la barbeta i representa l'equilibri perfecte entre comoditat i estil.",null),
("Metxes balayage", " el tint s'aplica amb pinzell ia mà alçada, uns centímetres per sota de l'arrel, aclarint els flocs de forma molt subtil fins a arribar a les puntes.",null),
("Metxes Californianes", "Es deixa l'arrel de cabell i mitjos amb el color natural i decolorar a partir dels mitjos fins a les puntes, aportant lluminositat i moviment al cabell de forma natural",null),
("Metxes Baby lights", "Són una coloració que simula els reflexos naturals que surten als nadons. S'aplica sobretot en cabelleres rosses i castanyes, per donar aquest toc ros al final dels cabells aportant aquesta brillantor extra",null),
("Modelat Desarrissat", "És un procés químic que modifica permanentment el cabell, de arrissat a llis",null),
("Allisat àcid hialurònic", " combina, com el seu nom indica, el famós àcid, a més àcid glioxílic i un conjunt d'aminoàcids que aporten sedositat i brillantor als cabells, eliminant l'encrespament",null),
("Tractament Ritual Express", "Tratmentant a base de keratina, que elimina el encrespament i el Frizz fins a un 50%.",null),
("Tractament Anticaiguda", "Tractament a base de vitamines, s'aplica com a tònics o mascaretes perquè els cabells tinguin un aspecte més sa i natural",null),
("Tractament Healthy color", "Aquesta tècnica, s'aplica reparació dels cabells sense sense productes químics agressius als cabells i la seva arrel",null),
("Sauna tradicional", "Genera habitualment calor humida gràcies a una estufa. L'índex d'humitat sol variar entre el 10% i el 25%.",null),
("Sauna d'infrarojos", "Produeix una calor seca gràcies a la seva tecnologia de panells infrarojos instal·lats uniformement a les parets de la cabina.",null),
("Tractament de pell amb revitalització facial", "Es duu a terme a través d'una mesoteràpia amb què s'arriba a augmentar la fermesa i la salut de la pell en aportar-te tots els nutrients i la hidratació que necessita.",null),
("Tractament de pell Carbon Peel Flash", "És l'opció perfecta si el que estàs buscant és una renovació poc invasiva i una estimulació del metabolisme per aconseguir superar les imperfeccions que el sol ha deixat a la cara.",null),
("Maquillatge de dia amb tocs ressaltats", "És més lleuger que el maquillatge nocturn, però també és una mica més carregat que el maquillatge de dia comú i corrent, i presenta tons més lluminosos on solen predominar els tons roses, daurats i marrons.",null),
("Maquillatge per a fotos", "Aquí pots optar per clàssics llapis de llavis de color vermell o rosat, acompanyat d'unes ombres per a parpelles fosques i un delineat del mateix to.",null),
("Massatge Relaxant Descontracturant", "L'objectiu del massatge relaxant és proporcionar un benestar general a l'organisme gràcies a les encefalines i endorfines que s'alliberen",null),
("Massatge Reafirmant Estimulant", "El massatge reafirmant, també anomenat estimulant o tonificant, ens ofereix una millora en el to muscular i del teixit cutani, especialment per a la prevenció i el tractament pal·liatiu de la flacciditat",null);

-- S'eliminen valors repetits, fent una comparació amb la mateixa taula a la columna, "nom_tipus_servei"
-- i comparant la grandaria del id_tipus_serveis
DELETE t1 FROM beauty.tipus_serveis t1 INNER JOIN beauty.tipus_serveis  t2
ON t2.nom_tipus_servei = t1.nom_tipus_servei AND t2.id_tipus_servei < t1.id_tipus_servei;
-- S'ahabilita el mode segur el motor mysql
SET SQL_SAFE_UPDATES = 1;
