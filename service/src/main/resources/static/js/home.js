


document.addEventListener("DOMContentLoaded", 
/**
/*S'afegeix l'esdeveniment load a la classe "menu-btn", ia la classe "mainMenu" se li 
*atribueix l'esdeveniment onclick
 * @param {String} event - Event a mostrar
 */
function (event) {
    document.querySelector('.menu-btn').addEventListener('click', () => document.querySelector('.main-menu').classList.toggle('show'));
});

/* Menu desplegable*/

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  // Close the dropdown menu if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }