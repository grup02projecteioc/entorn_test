window.addEventListener('load', ini);

function ini() {
   validarFormsListeners("registreClient");
}

/**
 * Funció que valido camps tipus text, number o mail
 * @param {String} id El id del element a validar
 * @returns {Boolean} depent de la valició retornara true o false
 */
function validarForms(id) {

   let isValid = true;
   const elements = document.getElementById(id).elements;
   for (var i = 0, element; element = elements[i++];) {
      if ((element.type === "text" || element.type === "number") && element.value.trim().length == 0) {
         isValid = false;

      } else {
         element.style.background = '';
      }

      if (element.type === "email") {
         const regex = /^((?!\.)[\w\-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/;
         if (!regex.test(element.value)) {
            isValid = false;

         } else {
            element.style.background = '';
         }

      }


   }
   return isValid;
}

/**
 * Funció que valida un form, al disparar-se el event submint
 * @param {String} formId Objecte a validar     
 */
function validarFormsListeners(formId) {
   document.getElementById(formId).addEventListener("submit", (event) => {
      let myForm = validarForms(formId);
      if (!myForm) {
         event.preventDefault();

      }
   }
   );
}