package beauty.service.service.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import beauty.service.service.repositori.CrudRepositoriTipusServei;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.TipusServeis;
import beauty.service.service.service.SaloService;
import beauty.service.service.service.TipusServeisService;

/**
 * Classe que representa el controlador que gestiona els Tipus de servei
 */
@Controller
public class ControladorTipusServei {

    @Autowired
    private TipusServeisService tipusServeisService;

    @Autowired
    private SaloService saloService;

    @Autowired
    CrudRepositoriTipusServei crudRepositoriTipusServei;

    /**
     * Mètode que recupera i llista tots els tipus de serveis carregats a la base de dades
     * @param model  Objecte tipus model per poder mostrar una vista
     * @return vista 'llistarTipusServei'
     */
    @GetMapping("/llistarRegistreGestorTipusServei")
    public String showRegistreGestorTipusServei(Model model) {
        List<TipusServeis> tipusServeis = (List<TipusServeis>) tipusServeisService.findAllServeis();
        model.addAttribute("llistarTipusServeis", tipusServeis);
        return "llistarTipusServei";
    }

    /**
     * Mètode que, mitjançant l'anotació ModelAttributte, rep les dades d'un tipus de servei cumplimentat
     * en un formulari i que crea un objecte de tipus TipusServei. Si no existeix encara, el guarda
     * a la base de dades.
     * @param tipusServeis El tipus de servie que s'envia al client
     * @param resultatValidacio Per validar els camps del costat del servidor
     * @param model Objecte tipus model per poder mostrar una vista
     * @return redirecció a la vista 'llistarRegistreGestorTipusServei'
     */
    @PostMapping("/registreTipusServeiProcess")
    public String addNewTipusServie(@ModelAttribute("tipusServeis") TipusServeis tipusServeis,
            BindingResult resultatValidacio, Model model) {

        model.addAttribute("nomTipusServei", tipusServeis.getNomTipusServei());
        model.addAttribute("descripcioTipus", tipusServeis.getDescripcioTipus());

        TipusServeis tipusServeisExistent = crudRepositoriTipusServei
                .getTipusServeiByNom(tipusServeis.getNomTipusServei());

        if (resultatValidacio.hasErrors() || tipusServeisExistent != null) {
            model.addAttribute("missatgeError", "Aquest tipus de servei ja està registrat");
            return "registreTipusServei";
        } else {
            tipusServeisService.guardarServeis(tipusServeis);
            return "redirect:/llistarRegistreGestorTipusServei";
        }
    }

    /**
     *Funció que mostra una vista a un gestor per afegir un tipus servei
     * @param model Objecte tipus model per poder mostrar una vista
     * @return Retorna un string amb el nom d'una vista "registreTipusServei"
     */
    @GetMapping("/showRegistreGestorTipusServei")
    public String mostrarFormTipusServie(Model model) {

        model.addAttribute("tipusServeis", new TipusServeis());
        return "registreTipusServei";
    }

    
    /** 
     * Mètode per registrar  un tipus servei 
     * @param model Objecte tipus model per poder mostrar una vista
     * @return String Retorna el nom de una vista "registreClientTipusServie"
     */
    @GetMapping("/showRegistreClienTipusServei")
    public String mostrarFormRegistreReserva(Model model) {
        List<TipusServeis> llistarTipusServeis = (List<TipusServeis>) tipusServeisService.findAllServeis();
        List<Salo> llistarSalons = (List<Salo>) saloService.getAllSalons();

        model.addAttribute("llistarTipusServeis", llistarTipusServeis);
        model.addAttribute("llistarSalons", llistarSalons);
        model.addAttribute("tipusServeis", new TipusServeis());
        model.addAttribute("salons", new Salo());
        return "registreClientTipusServie";
    }

/** 
 * Funció que recul dades d'un client a partir d'un formulari, per afegir un tipus servie i la seva descripción 
 * a la base de dades, a partir de paràmetres que s'envia al client. Depenent del estat de la validació
 * la funció torna a mostrar el el formulari o mostra un listat del tipus de servies i la seva descripció
 * @param tipusServeis El tipus de servie a recollir 
 * @param salo Objete de tipus Salo 
 * @param resultatValidacio Per validar el camps del costat del servidor
 * @param model La vista a retornar
 * @return String Retorna el una vista de nom "registreTipusServei"
 */
    @PostMapping("/registreClientTipusServeiProcess")
    public String addNewClientTipusServie(@ModelAttribute("tipusServeis") TipusServeis tipusServeis, Salo salo,
            BindingResult resultatValidacio, Model model) {

        model.addAttribute("nomTipusServei", tipusServeis.getNomTipusServei());
        model.addAttribute("descripcioTipus", tipusServeis.getDescripcioTipus());
        model.addAttribute("nomSalo", salo.getNomSalo());

        TipusServeis tipusServeisExistent = crudRepositoriTipusServei
                .getTipusServeiByNom(tipusServeis.getNomTipusServei());

        if (resultatValidacio.hasErrors() || tipusServeisExistent != null) {
            model.addAttribute("missatgeError", "Aquest tipus de servei ja està registrat");
            return "registreTipusServei";
        } else {
            tipusServeisService.guardarServeis(tipusServeis);
            return "redirect:/llistarRegistreGestorTipusServei";
        }

    }

}
