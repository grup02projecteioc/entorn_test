package beauty.service.service.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;
import beauty.service.service.repositori.Entitats.Usuari;
import beauty.service.service.service.SaloService;
import beauty.service.service.service.ServeisService;
import beauty.service.service.service.TipusServeisService;
import beauty.service.service.service.UsuariService;

/**
 * Classe que representa el controlador de la classe Salo
 */
@Controller
@ControllerAdvice
public class ControladorSalo extends HttpServlet {

    @Autowired
    private UsuariService usuariService;

    @Autowired
    private SaloService saloService;

    @Autowired
    private ServeisService serveisService;

    @Autowired
    private TipusServeisService tipusServeisService;

    /**
     * Mètode que rep com a paràmetre l'identificador del saló, el recupera amb un mètode del repositori
     * i introdueix totes les seves dades en una vista que retornarà a l'usuari de tipus gestor.
     * @param nomSalo
     * @param model
     * @param principal
     * @return vista "gestionarSalo"
     */
    @GetMapping(path="/gestionarSalo")
    public String gestionarSalo(@RequestParam(value = "idSalo") int idSalo, ModelMap model, Principal principal, HttpServletRequest request) {
        Usuari usuariLogat = usuariService.getUsuariByUsername(principal.getName());
        Salo salo = saloService.getSaloById(idSalo);
        List<Serveis> serveisSalo = serveisService.getServeisByIdSalo(salo.getIdSalo());
        model.addAttribute("serveis", serveisSalo);
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("nomSalo", salo.getNomSalo());
        model.addAttribute("idSalo", salo.getIdSalo());
        model.addAttribute("direccio", salo.getDireccio());
        model.addAttribute("codiPostal", salo.getCodiPostal());
        model.addAttribute("ciutat", salo.getCiutat());
        model.addAttribute("numTreballadors", salo.getNumTreballadors());

        request.getSession().setAttribute("serveisSalo", serveisSalo);
        
        return "gestionarSalo";
    }

    /**
     * Mètode que recupera un saló amb el seu identificador, que rep com a paràmetre, i introdueix en una 
     * vista tant els serveis escollits com els que encara no té seleccionats. 
     * @param idSalo
     * @param model
     * @param principal
     * @return vista "afegirMesServeis"
     */
    @GetMapping(path="/afegirMesServeis")
    public String afegirMesServeis(@RequestParam(value="idSalo") int idSalo, ModelMap model, Principal principal) {
        Usuari usuariLogat = usuariService.getUsuariByUsername(principal.getName());
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("idSalo", idSalo);
        List<Serveis> serveisActuals = serveisService.getServeisByIdSalo(idSalo);
        List<Integer> idTipusServeisActuals = new ArrayList<>();
        for(Serveis serveisAcInt : serveisActuals) {
            idTipusServeisActuals.add(serveisAcInt.getIdTipusServei());
        }
        model.addAttribute("serveisActuals", serveisActuals);
        List<TipusServeis> allServeis = tipusServeisService.findAllListTipusServeis();
        List<TipusServeis> serveisNoEscollits = tipusServeisService.llistatTipusServeisNoEscollits(idTipusServeisActuals, allServeis);
        model.addAttribute("serveis", serveisNoEscollits);

        return "afegirMesServeis";
    }

    /**
     * Mètode que recupera tots els valors seleccionats en checkboxes d'una vista. Accedeix a la taula
     * Serveis, esborra els serveis associats a aquest saló en concret i guarda els seleccionats, deixant
     * actualitzada la selecció de l'usuari gestor
     * @param idSalo
     * @param model
     * @param request
     * @return vista "gestionarSalo"
     */
    @RequestMapping(path="/afegirMesServeisProcess", method= RequestMethod.GET)
    public String afegirMesServeisProcess(@RequestParam(value="idSalo") int idSalo, ModelMap model, Principal principal, HttpServletRequest request) {
        serveisService.deleteServeisSalo(idSalo);
        Salo salo = saloService.getSaloById(idSalo);
        String[] valors = request.getParameterValues("servei");
        List<Serveis> serveisUsuari = new ArrayList<>();
        if (valors != null)  {
            for(int i=0; i<valors.length; ++i) {
                TipusServeis tipusServei = tipusServeisService.findTipusServeiByNom(valors[i]);
                String nomServei = tipusServei.getNomTipusServei();
                int idTipusServei = tipusServei.getIdTipusServei();
                Serveis servei = new Serveis();
                servei.setIdSalo(idSalo);
                servei.setIdTipusServei(idTipusServei);
                servei.setNomTipusServei(nomServei);
                serveisService.guardarServeis(servei);
                serveisUsuari.add(servei);
            }
        }
        model.addAttribute("nomUsuari", principal.getName());
        model.addAttribute("nomSalo", salo.getNomSalo());
        model.addAttribute("serveis", serveisUsuari);
        model.addAttribute("idSalo", idSalo);
        model.addAttribute("direccio", salo.getDireccio());
        model.addAttribute("codiPostal", salo.getCodiPostal());
        model.addAttribute("ciutat", salo.getCiutat());
        model.addAttribute("numTreballadors", salo.getNumTreballadors());

        return "gestionarSalo";
    }

    /**
     * Mètode que mostra el formulari que permet modificar les dades d'un saló, passant com a paràmetre
     * l'id del saló
     * @param idSalo
     * @param model
     * @param principal
     * @return vista formModificacioSalo
     */
    @RequestMapping(path="/modificarDadesSalo", method= RequestMethod.GET)
    public String mostrarDadesAModificarSalo(@RequestParam(value="idSalo") int idSalo, ModelMap model, Principal principal) {
        Salo salo = saloService.getSaloById(idSalo);
        String nomSalo = salo.getNomSalo();
        String direccio = salo.getDireccio();
        String codiPostal = salo.getCodiPostal();
        String ciutat = salo.getCiutat();
        int numTreballadors = salo.getNumTreballadors();

        model.addAttribute("idSalo", idSalo);
        model.addAttribute("nomUsuari", principal.getName());
        model.addAttribute("nomSalo", nomSalo);
        model.addAttribute("direccio", direccio);
        model.addAttribute("codiPostal", codiPostal);
        model.addAttribute("ciutat", ciutat);
        model.addAttribute("numTreballadors", numTreballadors);
        model.addAttribute("salo", new Salo());

        return "formModificacioSalo";
    }

    /**
     * Mètode que modifica les dades d'un saló emmagatzemat a la base de dades mitjançant un formulari,
     * passant com a paràmetre un objecte de tipus Salo creat a partir del formulari amb l'anotació
     * ModelAttribute
     * @param salo
     * @param model
     * @param principal
     * @return vista "gestionarSalo"
     */
    @RequestMapping(path="/modificacioSaloProcess", method = RequestMethod.POST)
    public String modificacioSaloProcess(@ModelAttribute("salo") Salo salo, ModelMap model, Principal principal) {
        model.addAttribute("nomUsuari", principal.getName());
        model.addAttribute("idSalo", salo.getIdSalo());

        String nomSaloCanviat = salo.getNomSalo();
        String direccioSaloCanviada = salo.getDireccio();
        String ciutatCanviada = salo.getCiutat();
        String cpCanviat = salo.getCodiPostal();
        int numTreballadorsCanviat = salo.getNumTreballadors();

        if (nomSaloCanviat.length() != 0) {
            salo.setNomSalo(nomSaloCanviat);
            model.addAttribute("nomSalo", salo.getNomSalo());
        }
        if (direccioSaloCanviada.length() != 0) {
            salo.setDireccio(direccioSaloCanviada);
            model.addAttribute("direccio", salo.getDireccio());
        }
        if (ciutatCanviada.length() != 0) {
            salo.setCiutat(ciutatCanviada);
            model.addAttribute("ciutat", salo.getCiutat());
        } 
        if (cpCanviat.length() != 0) {
            salo.setCodiPostal(cpCanviat);
            model.addAttribute("codiPostal", salo.getCodiPostal());
        }
        if (numTreballadorsCanviat != 0) {
            salo.setNumTreballadors(numTreballadorsCanviat);
            model.addAttribute("numTreballadors", salo.getNumTreballadors());
        }
        model.addAttribute("nomSalo", salo.getNomSalo());
        model.addAttribute("direccio", salo.getDireccio());
        model.addAttribute("codiPostal", salo.getCodiPostal());
        model.addAttribute("ciutat", salo.getCiutat());
        model.addAttribute("numTreballadors", salo.getNumTreballadors());

        return "gestionarSalo";
    }

}
