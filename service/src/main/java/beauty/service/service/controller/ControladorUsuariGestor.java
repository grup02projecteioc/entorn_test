package beauty.service.service.controller;


import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import beauty.service.service.service.SaloService;
import beauty.service.service.service.ServeisService;
import beauty.service.service.service.TipusServeisService;
import beauty.service.service.service.UsuariService;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;
import beauty.service.service.repositori.Entitats.Usuari;

/**
 * Classe que representa el controlador que gestiona l'usuari de tipus gestor.
 * Conté mètodes que són utilitzats pels dos tipus d'usuaris (gestor i client)
 */
@Controller
public class ControladorUsuariGestor {
    
    @Autowired
    private UsuariService service;

    @Autowired
    private SaloService serviceSalo;

    @Autowired
    private TipusServeisService serviceTipusServeis;

    @Autowired
    private ServeisService serveisService;

    /**
     * Mètode que dóna d'alta un usuari gestor a la base de dades completant les dades en un formulari, 
     * mitjançant l'anotació @ModelAttribute. Si ja existeix el e-mail o el nom d'usuari, no deixa
     * registrar-se i torna de nou el formulari amb un missatge d'error
     * @param usuariGestor
     * @param model
     * @return vista 'registreOK'
     */
    @RequestMapping(path="/registreGestorProcess", method= RequestMethod.POST)
    public String addNewUsuariGestor(@ModelAttribute("usuariGestor") Usuari usuariGestor, ModelMap model) {
        Usuari mailUsuariExistent = service.getUsuariByEmail(usuariGestor.getMail());
        Usuari nomUsuariExistent = service.getUsuariByUsername(usuariGestor.getNomUsuari());
        
        if (mailUsuariExistent == null && nomUsuariExistent == null) {

            service.guardaUsuari(usuariGestor);

            return "registreOK";
        }
        if (mailUsuariExistent != null) {
            model.addAttribute("missatgeMail", "Ja existeix un usuari amb aquest mail");
        }
        if (nomUsuariExistent != null) {
            model.addAttribute("missatgeNomUsuari", "Nom d'usuari no disponible");
        }
        

        return "registreGestorCentre";

    }

    /**
     * Mètode que introdueix en una vista les dades de l'usuari logat. Aquesta vista conté text-boxes
     * editables que permet modificar les dades.
     * @param model
     * @param principal
     * @return vista 'formModificacio'
     */
    @GetMapping("/modificarDades")
    public String mostrarDadesAModificar(ModelMap model, Principal principal) {
        String nomUsuariLogat = principal.getName();
        Usuari usuariLogat = service.getUsuariByUsername(nomUsuariLogat);
        String nom = usuariLogat.getNom();
        String cognom = usuariLogat.getCognoms();
        String ciutat = usuariLogat.getCiutat();
        String codiPostal = usuariLogat.getCodiPostal();

        model.addAttribute("idUsuari", usuariLogat.getIdUsuari());
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("nom", nom);
        model.addAttribute("cognoms", cognom);
        model.addAttribute("ciutat", ciutat);
        model.addAttribute("codiPostal", codiPostal);
        model.addAttribute("usuariGestor", new Usuari());

        return "formModificacio";
        
    }

    /**
     * Mètode que processa les dades que rep del formulari de modificació de dades de l'usuari. Si una ç
     * dada està cumplimentada, la substitueix. Si l'usuari ha esborrat la dada del camp de text, el mètode 
     * no substitueix la informació i la deixa sense modificar a la base de dades.
     * @param usuariGestor
     * @param model
     * @param principal
     * @return vista 'lesMevesDades'
     */
    @RequestMapping(path="/modificacioUsuariProcess", method= RequestMethod.POST)
    public String modificacioUsuariProcess(@ModelAttribute("usuariGestor") Usuari usuariGestor, ModelMap model, Principal principal) {
        Usuari usuariLogat = service.getUsuariByUsername(principal.getName());
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("mail", usuariLogat.getMail());
        model.addAttribute("idUsuari", usuariLogat.getIdUsuari());
        model.addAttribute("rol", usuariLogat.getRol());

        String nomCanviat = usuariGestor.getNom();
        String cognomsCanviats = usuariGestor.getCognoms();
        String ciutatCanviada = usuariGestor.getCiutat();
        String codiPostalCanviat = usuariGestor.getCodiPostal();

        if (nomCanviat.length() != 0) {
            usuariLogat.setNom(nomCanviat);
            model.addAttribute("nom", usuariLogat.getNom());
        }
        if (cognomsCanviats.length() != 0) {
            usuariLogat.setCognoms(cognomsCanviats);
            model.addAttribute("cognoms", usuariLogat.getCognoms());
        }
        if (ciutatCanviada.length() != 0) {
            usuariLogat.setCiutat(ciutatCanviada);
            model.addAttribute("ciutat", usuariLogat.getCiutat());
        } 
        if (codiPostalCanviat.length() != 0) {
            usuariLogat.setCodiPostal(codiPostalCanviat);
            model.addAttribute("codiPostal", usuariLogat.getCodiPostal());
        }
        
        service.guardaUsuari(usuariLogat);

        return "lesMevesDades";
    }

    /**
     * Mètode que guarda un saló a la base de dades i fa una redirecció a la ruta que permet veure el
     * detall del saló creat, sempre i quan l'usuari no tingui un saló ja registrat en la misma direcció
     * @param salo
     * @param model
     * @param principal
     * @return vista "nouSalo" o redirecció /gestionarSalo
     */
    @RequestMapping(path="/registreSaloProcess", method= RequestMethod.POST)
    public String addNewSalo(@ModelAttribute("salo") Salo salo, ModelMap model, Principal principal, HttpServletRequest request) {
        Usuari usuariLogat = service.getUsuariByUsername(principal.getName());
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        List<Salo> salonsUsuari = serviceSalo.getSalonsByIdUsuari(usuariLogat.getId());
        Salo saloDireccioExistent = serviceSalo.getSaloByDireccio(salonsUsuari, salo.getDireccio());       
        if(saloDireccioExistent == null) {
            serviceSalo.guardarSalo(salo);
            salo.setUsuari(usuariLogat);
            String[] valors = request.getParameterValues("servei");
            List<Serveis> serveisUsuari = new ArrayList<>();
            if (valors != null) {
                for (int i=0; i<valors.length; ++i) {
                    TipusServeis tipusServei = serviceTipusServeis.findTipusServeiByNom(valors[i]);
                    String nomServei = tipusServei.getNomTipusServei();
                    int idTipusServei = tipusServei.getIdTipusServei();
                    int idSalo = salo.getIdSalo();
                    Serveis servei = new Serveis();
                    servei.setIdSalo(idSalo);
                    servei.setIdTipusServei(idTipusServei);
                    servei.setNomTipusServei(nomServei);
                    serveisService.guardarServeis(servei);
                    serveisUsuari.add(servei);
                }
            }
            model.addAttribute("nomSalo", salo.getNomSalo());
            model.addAttribute("serveis", serveisUsuari);
            model.addAttribute("idSalo", salo.getIdSalo());
            model.addAttribute("direccio", salo.getDireccio());
            model.addAttribute("codiPostal", salo.getCodiPostal());
            model.addAttribute("ciutat", salo.getCiutat());
            model.addAttribute("numTreballadors", salo.getNumTreballadors());
            model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
     
            return "redirect:/gestionarSalo?idSalo=" + salo.getIdSalo();
        }
        List<TipusServeis> allServeis = serviceTipusServeis.findAllListTipusServeis();
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("serveis", allServeis);
        model.addAttribute("missatgeDireccio", "Ja tens un saló registrat en aquesta direcció");

        return "nouSalo";
    }
 
    /**
     * Mètode que recupera tots els salons registrats per l'usuari gestor a la base de dades
     * i els llista per introduir-los en una vista.
     * @param model
     * @param principal
     * @return 'elsMeusSalons'
     */
    @GetMapping(value="/elsMeusSalons")
    public String llistatSalons(ModelMap model, Principal principal) {
        Usuari usuariLogat = service.getUsuariByUsername(principal.getName());
        List<Salo> salonsUsuari = serviceSalo.getSalonsByIdUsuari(usuariLogat.getId());
        model.addAttribute("salons", salonsUsuari);
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());

        return "elsMeusSalons";
    }

    
    @GetMapping("/danarDeBaixaSalo")
    public String danarDeBaixaSalo(Model models, HttpServletRequest request) throws ServletException{
        final String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
       service.danarDeBaixaSalo(currentUserName);
       request.logout();
      return "home";
    }

    @GetMapping("/usuariactual")
    public String usuariactual(Model model) {
        final String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("currentUserName",currentUserName);
      return "home";
    } 


}
