package beauty.service.service.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Classe on es configura Spring Security
 */
@Configuration
@EnableWebSecurity 
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Bean
    public UserDetailsService userDetailsService() {
        return new MyUserDetailsService();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        return authProvider;
    }
 
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/admin/**").hasRole("ADMIN")
            .antMatchers("/login*").permitAll()
            .antMatchers("/").permitAll()
            .antMatchers("/showRegistre*").permitAll()
            .antMatchers("/tipusUsuari").permitAll()
            .antMatchers("/regis*").permitAll()
            .antMatchers("/css/*").permitAll()
            .antMatchers("/img/*").permitAll()
            .antMatchers("/js/*").permitAll()
            .antMatchers("/llistar*").permitAll() 
            .antMatchers("/buscar*").permitAll()
            .antMatchers("/cer*").permitAll()
            .antMatchers(" /api/*").permitAll()
            .antMatchers("/logout").permitAll()
            .antMatchers("/quiSom").permitAll()
            .anyRequest().authenticated()          
            .and()
            .formLogin()
            .loginPage("/login")
            .usernameParameter("nomUsuari")
            .passwordParameter("contrasenya")
            .defaultSuccessUrl("/benvingut", true)
            .and()
            .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));  

            http.headers()
            .frameOptions().sameOrigin()
            .httpStrictTransportSecurity().disable();
             http.csrf().disable();
            //sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().
            //authorizeRequests().and();
            //exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint());           

    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    

    
}

