package beauty.service.service.security;

import java.util.Arrays;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import beauty.service.service.service.UsuariService;
import beauty.service.service.repositori.Entitats.Usuari;

/**
 * Classe que implementa la interfície UserDetails, i que permet recuperar les
 * dades de login correctes d'un usuari registrat, a més comprova si està actiu
 */
public class MyUserDetails implements UserDetails {

    private Usuari usuari;
    
    public MyUserDetails(Usuari usuari) {
        this.usuari=usuari;
    }

    public MyUserDetails() {

    }

    @Autowired
    UsuariService usuarisService;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(usuari.getRol());
        return Arrays.asList(authority);
        //return null;
    }

    @Override
    public String getPassword() {
  
        String pass=  usuari.getContrasenya();
        return "{noop}" + pass;
    }

    @Override
    public String getUsername() {
        
        return usuari.getNomUsuari();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return usuari.isActive();
    }
    
}

