package beauty.service.service.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.RepositoriEvent;
import beauty.service.service.repositori.Entitats.Event;

/**
 * Classe que registre events. Basicament, enregistre mètodes per donar suport a la
 * assistència de dades a l'hora de triar un event
 */
@Service
@Transactional
public class EventService {
    

    @Autowired
    private RepositoriEvent repositoriEvent;

    /***
     * Mètode que retorna un Event passant com a paràmetre
     * l'identificador d'aquest
     * @param idEvent Id per trobar un event donat
     * @return Event Retorna un objecte tipus Event
     */
    public Event getEventById(int idEvent) {
        return repositoriEvent.getEventById(idEvent);
    }

    /***
     * Mètode que retorna tot el llistat d'events guardats a la 
     * base de dades
     * @return Iterable<Event> Retorna un llistat d'objectes tipus Event
     */
    public List<Event> getAllEvents() {
        return (List<Event>)repositoriEvent.getAllEvents();
    }


}
