package beauty.service.service.repositori;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;

/**
 * Classe que representa el repositori de la classe TipusServeis
 */
@Repository
public class RepositoriTipusServeis {
    
    @Autowired
    CrudRepositoriTipusServei crudRepositoriTipusServei;

    /**
     * Mètode que retorna un TipusServeis entre el llistat de tots els 
     * TipusServeis carregats a la base de dades passant com a paràmetre el nom
     * d'un TipusServeis
     * @param nom
     * @return TipusServeis
     */
    public TipusServeis findTipusServeiByNom(String nom) {
        Iterable<TipusServeis> allTipusServeis = crudRepositoriTipusServei.findAll();
        for(TipusServeis ts: allTipusServeis) {
            if(ts.getNomTipusServei().equals(nom)) {
                return ts;
            }
        }
        return null;
    }

    /**
     * Mètode que retorna un llistat de Id de TipusServeis passant com a 
     * paràmetre un llistat de Serveis 
     * @param serveis
     * @return List<Integer>
     */
    public List<Integer> llistatIdTipusServeis(List<Serveis> serveis) {
        List<Integer> idsTipusServeis = new ArrayList<>();
        for (int i=0; i<serveis.size(); i++) {
            idsTipusServeis.add(serveis.get(i).getIdTipusServei());
        }

        return idsTipusServeis;
    }

    /**
     * Mètode que retorna un llistat de TipusServeis que l'usuari de tipus gestor encara no té assignats
     * a un saló en concret a la base de dades, passant com a paràmetres un llistat d'identificadors ja escollits
     * i el llistat de tots els TipusServeis carregats a la base de dades.
     * @param idsTipusServeisEscollits
     * @param allTipusServeis
     * @return llistat TipusServeis
     */
    public List<TipusServeis> llistatTipusServeisNoEscollits(List<Integer> serveisEscollits, List<TipusServeis> allTipusServeis) {
        List<TipusServeis> serveisNoEscollits = new ArrayList<>();
        for(TipusServeis element : allTipusServeis) {
           if(!serveisEscollits.contains(element.getIdTipusServei())) {
            serveisNoEscollits.add(element);
           }
        }
        return serveisNoEscollits;
    }

    /**
     * Mètode que passant com a paràmetre un identificar de TipusServeis retorna un tipus de servei
     * si aquest existeix
     * @param idTipusServeis
     * @return un TipusServei o null si no existeix
     */
    public TipusServeis getTipusServeiById(int idTipusServeis) {
        List<TipusServeis> allTipusServeis = (List<TipusServeis>)crudRepositoriTipusServei.findAll();
        for(TipusServeis ts: allTipusServeis) {
            if(ts.getIdTipusServei() == idTipusServeis) {
                return ts;
            }
        }

        return null;
    }
}
