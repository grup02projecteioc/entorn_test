package beauty.service.service.repositori.Entitats;

/**
 * Classe que representa un formulari de login d'usuari.
<p>Els mètodes es faran servir per actualitzar i obtenir propietats a l’hora de donar acces algun tipus d’usuari. 
Els usuaris acceptats són: usuari client i usuari gestor</p>
 */
public class Login {
    private String username;
    private String hashedPassword;

    /**
     * <p>Funció que opte el nom d'un usuari, gestionant així l'accés a la pàgina web.</p>
     * @return String Retorna un String, que en aquest cas, és el nom d’un usuari
     */
    public String getNomUsuari() {
        return username;
    }

    /**
     * <p>Mètode que actualitza el nom d’un usuari, 
     * passant-li per paràmetre un objecte tipus String, que representa el nom d’un usuari</p>
     * @param nomUsuari Objecte tipus String, que representa  l’usuari a actualitzar.
     */
    public void setNomUsuari(String nomUsuari) {
        this.username = nomUsuari;
    }

    /**
     * <p>Funció per obtenir la contrasenya d’un usuari loguejat</p>
     * @return String Ens retorna un String per poder obtenir la contrasenya 
     * d’un usuari registrat dins el sistema
     */
    public String getContrasenya() {
        return hashedPassword;
    }

    /**
     * <p>Mètode per actualitzar la contrasenya d’un usuari registrat dins el sistema</p>
     * @param contrasenya Objecte tipus String que és la contrasenya a actualitzar
     */
    public void setContrasenya(String contrasenya) {
        this.hashedPassword = contrasenya;
    }
}
