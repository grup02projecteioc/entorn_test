package beauty.service.service.repositori;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import beauty.service.service.repositori.Entitats.Usuari;

//Interfície que crida a CrudRepository
public interface CrudRepositoriUsuari extends CrudRepository<Usuari, Integer> {
    @Query("SELECT u FROM Usuari u WHERE u.nomUsuari = ?1")
    public Usuari getUsuariByUsername(String username);

    @Query("SELECT u.idUsuari FROM Usuari u WHERE u.active=1 AND u.rol='GESTOR' AND u.nomUsuari = ?1")
    public int getIdUsuariByUsername(String username);
}

