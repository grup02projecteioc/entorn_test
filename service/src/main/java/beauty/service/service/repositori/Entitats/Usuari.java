package beauty.service.service.repositori.Entitats;


import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa una entitat de tipus Usuari </h1>
 * <p>Aquesta entitat gestionarà tipus d'usuaris amb dos rols predeterminats; "GESTOR" i "CLIENT"</p>
 */
@Entity
public class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;



    @OneToMany(mappedBy = "usuari",fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private Set<Reserves> reserves;
    
    @OneToMany(mappedBy = "usuari",fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private Set<Salo> salo;

    @Id
    @NotNull
    @Column(name = "idUsuari")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=9)
    private int idUsuari;

    @NotNull
    @Column(name = "nom")
    @Size(max=20)
    private String nom;

    @NotNull
    @Column(name = "cognoms")
    @Size(max=45)
    private String cognoms;

    @Column(name="mail")
    @Size(max=45)
    @NotNull
    @Email
    private String mail;

    @NotNull
    @Column(name = "nomUsuari")
    @Size(max=20)
    private String nomUsuari;

    @Column(name="contrasenya")
    @Size(max=45)
    @NotNull
    private String contrasenya;


    @Column(name="ciutat")
    @Size(max=45)
    @NotNull
    private String ciutat;

    @Column(name="codiPostal")
    @Size(max=5)
    @NotNull
    private String codiPostal;


    @Column(name="rol")
    @NotNull
    private String rol;

  
    @Column(name = "active")
    @NotNull
    private Boolean active=true;


   /**
     * Constructor buit {@link Usuari} , que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     * duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes
     */    
    public Usuari() {}
    
/**
 * Constructor per obtenir instacias de tipus {@link Usuari}
 * @param nom Paràmetre de tipus String, el qual se li assignarà un nom d'usuari.
 * @param cognoms Paràmetre per assignar els cognoms d'un usuari
 * @param mail Amb aquesta entrada, cada usuari tendra una adreça  electrònica
 * @param nomUsuari Valor per establir un user dins del sistema de reserves de servies.
 * @param contrasenya Valor secret que cada usuari tindrà
 * @param ciutat Ciutat de cada usuari
 * @param codiPostal Amb aquesta entra en servira per fer una reserva  perquè un usuari pugui triar un tipus de servei.
 * @param rol Els usuraris seran de dos tipus; "GESTOR" i "CLIENT"
 * @param active Amb aquest tindre dos valor 1 si un usuari està actiu i 0 si un usuari s'ha donat de baixa
 */
    public Usuari( String nom, String cognoms, String mail, String nomUsuari, String contrasenya, String ciutat, String codiPostal, String rol, Boolean active) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.mail = mail;
        this.nomUsuari = nomUsuari;
        this.contrasenya = contrasenya;
        this.ciutat = ciutat;
        this.codiPostal = codiPostal;
        this.rol = rol;
        this.active = active;
    }


    //Getters i setters

    public Set<Salo> getSalo() {
        return this.salo;
    }

    public void setSalo(Set<Salo> salo) {
        this.salo = salo;
    }


    public int getId() {
        return idUsuari;
    }



    public void setId(int id) {
        this.idUsuari = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNomUsuari() {
        return nomUsuari;
    }

    public void setNomUsuari(String nomUsuari) {
        this.nomUsuari = nomUsuari;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }


    public Boolean isActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    

    public void setReserves(Set<Reserves> reserves) {
        this.reserves = reserves;
    }

    public Set<Reserves> getReserves() {
        return this.reserves;
    }


    public Boolean getActive() {
        return this.active;
    }



    public int getIdUsuari() {
        return this.idUsuari;
    }

    public void setIdUsuari(int idUsuari) {
        this.idUsuari = idUsuari;
    }



}

