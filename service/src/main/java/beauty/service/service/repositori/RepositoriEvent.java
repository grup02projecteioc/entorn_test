package beauty.service.service.repositori;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import beauty.service.service.repositori.Entitats.Event;

/**
 * Classe que representa el repository de la classe Event
 */
@Repository
public class RepositoriEvent {
    
    @Autowired
    private EventRepository crudEventRepository;

    /**
     * Mètode que rep com a paràmetre l'id d'un Event i el recupera, fent una
     * iteració sobre tots els Event registrats a la base de dades
     * @param idEvent Per trobar un Event en concret
     * @return l'Event o null si aquest no existeix
     */
    public Event getEventById(int idEvent) {
        Iterable<Event> allEvents = crudEventRepository.findAll();
        for(Event e: allEvents) {
            if(e.getId()==idEvent) {
                return e;
            }
        }

        return null;
    }

    /**
     * Mètode que crida un mètode de la interfície CrudEventRepostori
     * per recuperar tots els Events registrats a la base de dades
     * @return Iterable d'Event
     */
    public Iterable<Event> getAllEvents() {
        return crudEventRepository.findAll();
    }
}
