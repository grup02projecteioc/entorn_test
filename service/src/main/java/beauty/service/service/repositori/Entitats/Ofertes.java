package beauty.service.service.repositori.Entitats;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Ofertes")
public class Ofertes implements Serializable {
    /**
     *
     */
    private static final String ID_OFERTES = "id_ofertes";

    private static final long serialVersionUID = 1L;

    
    @OneToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "idReserva")
    private Reserves reserva;
   // @JoinColumn(name = "idEvent")
    //private Event event;

    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Salo salo;

    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "idServei") 
    private Serveis serveis;
    

    @Id
    @Column(name="idOferta")
    @Size(max=11)
    @NotNull
    private int idOferta;

    @NotNull
    @Column(name = "nifSalo")
    @Size(max=45)
    private String nifSalo;

    @NotNull
    @Column(name = "idServicio")
    @Size(max=11)    
    private int idServicio;

    @Column(name = "descripcio")
    @Size(max=120)    
    private String descripcio;

    @NotNull
    @Column(name = "data")
    private Date data;

    @NotNull
    @Column(name = "tramHorari")
    @Size(max=45)   
    private String tramHorari;

    @NotNull
    @Column(name = "preu")   
    private double preu;

    //Getters i Setters

    public Ofertes(String nifSalo, int idServicio, String descripcio, Date data, String tramHorari, double preu) {
        this.nifSalo = nifSalo;
        this.idServicio = idServicio;
        this.descripcio = descripcio;
        this.data = data;
        this.tramHorari = tramHorari;
        this.preu = preu;
    }

    
    public Ofertes() {
    }

/*
    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

*/

    public Reserves getReserva() {
        return this.reserva;
    }

    public void setReserva(Reserves reserva) {
        this.reserva = reserva;
    }

    public Salo getSalo() {
        return this.salo;
    }

    public void setSalo(Salo salo) {
        this.salo = salo;
    }

    public Serveis getServeis() {
        return this.serveis;
    }

    public void setServeis(Serveis serveis) {
        this.serveis = serveis;
    }


    
    public int getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    public String getNifSalo() {
        return nifSalo;
    }

    public void setNifSalo(String nifSalo) {
        this.nifSalo = nifSalo;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getTramHorari() {
        return tramHorari;
    }

    public void setTramHorari(String tramHorari) {
        this.tramHorari = tramHorari;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    
}
